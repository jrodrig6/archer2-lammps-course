<img src="./Images/Archer2_logo.png"  width="355" height="100" align="left"> <img src="./Images/epcc_logo.jpg" align="right" width="133" height="100">

<br /><br /><br /><br /><br />


# ARCHER 2 LAMMPS course (May 2020)

[![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

LAMMPS (Large-scale Atomic/Molecular Massively Parallel Simulator) is a widely-used classical molecular dynamics (MD) code. This C++ code is easy to use, incredibly versatile, and parallelised to run efficiently on both small-scale personal computers and CPU/GPU/CPU&GPU HPC clusters. As of 2018, LAMMPS has been used, to some degree, in over 14,000 publications in fields as varied as chemistry, physics, material science, granular and lubricated-granular flow, etc.

The course will be run over three 2.5 hour-long sessions.

The first session will be an introduction to setting up and running an MD simulation using LAMMPS. We will begin by running a simulation of a Lennard-Jones fluid before delving deeper into how simulations can be set up and run in LAMMPS.

In the second session, we will discuss how to download and install LAMMPS, with a more in-depth discussion of the various packages LAMMPS offers and how to use them efficiently.

The third session will be a follow-up session for exercises, discussion and questions.

## Course timetable

### Day 1

 * 14:00 Welcome and overview
 * 14:10 Introduction to setting up and running an MD simulation using LAMMPS. We will begin by running a simulation of a Lennard-Jones fluid.
 * 15:00 Break
 * 15:30 Delving deeper into how simulations can be set up and run in LAMMPS
 * 16:30 Close

### Day 2


 * 14:00 Welcome and recap
 * 14:10 How to download and install LAMMPS
 * 15:00 Break
 * 15:30 LAMMPS packages and how to use them efficiently
 * 16:30 Close

### Day 3

 * 14:00 Welcome and recap
 * 14:10 Exercise class with instructor present to address any questions
 * 16:00 Formal solutions to problems
 * 16:30 Close

## Course requirements

All attendees will need their own desktop or laptop .

If you are logging on to an external system then you will need to have an ssh client installed which comes as default for Linux and Mac systems.

Linux users should open a command-line terminal and use ssh from the command line.

Mac can open the Mac termimal application and use ssh from the command line. However, to display graphics from Cirrus you will also need to install Xquartz. Xquartz provides its own terminal called “Xterm”: if you have problems displaying graphics when using the Mac terminal, try logging in using ssh from within this Xterm.

Windows users should install MobaXterm which provides ssh access, a Unix graphics client and a drag-and-drop file browser.

We will provide accounts on one of our HPC services, for all attendees who register in advance at http://www.archer.ac.uk/training/registration/.

---

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

