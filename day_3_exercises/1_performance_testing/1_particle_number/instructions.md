# Effect of particle number on simulation performance

In this section of the exercise, you will be looking at how changing the number of particles in the simulation box affects the performance of LAMMPS. In general, increasing the number of particles in an MD simulation will increase the simulation time. Here, we will be looking more specifically at the breakdown in the times LAMMPS reports spending on various tasks within our simulation.

## Outputting LAMMPS performance data

When a simulation run is finished, LAMMPS will output a lot of interesting information about how much time was spent performing various parts of the simulation. For the simulations run in all of these exercises, this information can be found at the end of the log file (and after every "run" command is complete). You can use the ``grep`` command to output only the information of interest. For these exercises, running ```grep -A 40 'Loop time' log.lammps``` will output something similar to the breakdown shown below:

```
Loop time of 5.46669 on 24 procs for 50000 steps with 1000 atoms

Performance: 3951203.693 tau/day, 9146.305 timesteps/s
99.5% CPU use with 24 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 2.0514     | 2.241      | 2.4128     |   8.5 | 40.99
Neigh   | 0.49278    | 0.51746    | 0.54735    |   2.1 |  9.47
Comm    | 2.1666     | 2.3772     | 2.6018     |   9.7 | 43.49
Output  | 0.0017371  | 0.0017934  | 0.0027206  |   0.5 |  0.03
Modify  | 0.22781    | 0.25352    | 0.26602    |   1.7 |  4.64
Other   |            | 0.07566    |            |       |  1.38

Nlocal:    41.6667 ave 51 max 35 min
Histogram: 3 2 3 4 1 6 3 0 1 1
Nghost:    944.708 ave 960 max 929 min
Histogram: 1 2 1 3 5 3 5 1 1 2
Neighs:    2853.96 ave 3748 max 2423 min
Histogram: 7 3 2 5 1 3 0 2 0 1

Total # of neighbors = 68495
Ave neighs/atom = 68.495
Neighbor list builds = 4999
Dangerous builds = 4994
Total wall time: 0:00:05

```

This is a quite detailed output of LAMMPS performance information. Here, we will only look at a couple of parts of this output -- if you are interested in finding out more, you can read about it in the [LAMMPS user manual](https://lammps.sandia.gov/doc/Run_output.html).

Of specific interest to us are the loop time (the total amount of walltime that the simulation took -- in the example above, this is 5.47 s) and the MPI task timing breakdown (a breakdown of the minimum, average, and maximum times the processors spent on various parts of the program, as well as breakdown of the percentage of the total walltime that was spent on each task). The MPI task timing breakdown gives information for the following:

 * Pair -- the time taken to calculate pairwise particle interactions (Lennard-Jones interactions in this case)
 * Neigh -- the time taken to construct neighbour lists
 * Comm -- the time spent in inter-processor communications of particle positions and properties
 * Output -- the time spent outputting thermodynamic info and dump files
 * Modify -- the time spent on fixes and computes
 * Other -- all time spent not in any of the above

## Exercise question

For this exercise, we want to look at how the performance of the simulation changes as the number of particles is changed. From the Loop time information, we can see that the example output above is for a simulation system run using 1000 particles on 24 processors. This is a poor atoms/processor ratio -- each processor has roughly 40 atoms to work with and so a lot of time has been spent communicating with other processors. As a result, the main performance cost is communication (currently at 43.5% of the total simulation time) -- this is an inefficient use of processor number.

You have been provided an example LAMMPS input script `in.atom_number`. At the top, there is a variable `BOX_LENGTH` that defines how many atoms long each box side should be (so the total number of particles in the box is BOX_LENGTH<sup>3</sup>). What happens to the communication time as you vary the total number of particles? What about the overall simulation time?

Note: you probably do not want to exceed 125,000 particles on a single 24-core node on ARCHER. This already takes more than 7 mins to run.