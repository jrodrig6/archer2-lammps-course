####################################
# Example LAMMPS input script      #
# for a simple Lennard Jones fluid #
####################################

####################################
# 0) Define variables
####################################

variable	DENSITY equal 0.6
variable	BOX_LENGTH equal 10
variable	HALF_BOX_LENGTH equal 1.0*${BOX_LENGTH}/2.0

variable	THERMO_DELAY equal 500

variable	TOT_ATOM_NUMBER equal ${BOX_LENGTH}*${BOX_LENGTH}*${BOX_LENGTH}
variable	ANION_NUMBER equal  50
variable	CATION_NUMBER equal 50 
variable	NEUTRAL_NUMBER equal ${TOT_ATOM_NUMBER}-${ANION_NUMBER}-${CATION_NUMBER}

####################################
# 1) Set up simulation box
#   - We set a 3D periodic box
#   - Our box has 10x10x10 atom 
#     positions, evenly distributed
#   - The atom starting sites are
#     separated such that the box density
#     is 0.6
####################################

units		lj
atom_style	full
dimension	3
boundary	p p p

lattice		sc ${DENSITY}
region		box block 0 ${BOX_LENGTH} 0 ${BOX_LENGTH} 0 ${BOX_LENGTH}
create_box	3 box
create_atoms	1 random ${NEUTRAL_NUMBER} 871837 NULL
create_atoms	2 random ${ANION_NUMBER}   137154 NULL
create_atoms	3 random ${CATION_NUMBER}  687184 NULL 

####################################
# 2) Neighbour lists
#   - Each atom will only consider neighbours
#     within a distance of 2.8 of each other
#   - The neighbour lists are recalculated
#     every timestep
####################################

neighbor        0.3 bin
neigh_modify    delay 0 every 1

####################################
# 3) Relax simulation
#   - Atoms have been randomly packed
#     which can lead to overlaps
#   - This bit of code is to allow these
#     overlaping atoms to move away from
#     one another without box explosions
####################################

thermo_style    custom step temp density epair etotal
thermo          1000
compute_modify  thermo_temp dynamic yes

pair_style      soft 1.12246
pair_coeff      * * 0.0
mass		* 1.0

fix             1 all nve
variable        prefactor equal ramp(1.0,100.0)
fix             SOFT all adapt 1 pair soft a * * v_prefactor

timestep        5e-4

run 2500

unfix		1
unfix		SOFT

####################################
# 4) Define interparticle interactions
#   - Here, we use truncated & shifted LJ
#   - All atoms of type 1 (in this case, all atoms)
#     have a mass of 1.0
####################################

kspace_style	pppm 0.001
pair_style	lj/cut/coul/long 2.5 ${HALF_BOX_LENGTH}
pair_coeff	* * 1.0 1.0
mass		* 1.0
set		type 2 charge -1.00
set		type 3 charge 1.00


####################################
# 5) Define simulation parameters
#   - We fix the temperature and 
#     linear and angular momenta
#     of the system 
#   - We run with fixed number (n),
#     volume (v), temperature (t)
####################################

fix		LinMom all momentum 50 linear 1 1 1 angular
fix		1 all nvt temp 1.00 1.00 5.0

####################################
# 6) Final setup
#   - Define starting particle velocity
#   - Define timestep
#   - Define output system properties (temp, energy, etc.)
#   - Define simulation length
####################################

velocity	all create 1.0 199085 mom no

timestep	0.005

thermo_style	custom step temp etotal pe ke ecoul press vol density
thermo		${THERMO_DELAY}

dump           2 all custom 1000 positions.lammpstrj id type x y z vx vy vz
dump_modify    2 sort id

run_style	verlet

run		50000
