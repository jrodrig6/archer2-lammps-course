# Preparing LAMMPS data files

In these files, you will find instructions for one way of preparing a LAMMPS data file. For this, you will need to have [Packmol](http://m3g.iqm.unicamp.br/packmol/home.shtml) and [VMD](https://www.ks.uiuc.edu/Research/vmd/) installed.

 1. Run the Packmol input `xyz.packmol.inp` in your Packmol executable
 2. Open the packed system `PACKED_WATER.xyz` using VMD
 3. Run the commands in `TOPO_README` in VMD (either in the shell input line or using 'Extensions' -> 'Tk console')
 4. In your `TIP4P.data` file, you will need to update the lower and upper bounds of your simulation box. When doing this, bear in mind that your simulations will be run using periodic boundary conditions.
