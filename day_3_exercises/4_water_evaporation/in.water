##################################################################################
#                             Variable to change                                 #
##################################################################################

variable	TEMPERATURE equal 275.0 # Temperature in K
variable	LAST_TEMP   equal 275.0 #Temperature of last run
variable	PRESSURE    equal 1.0  # Pressure in atm 
variable	TIMESTEP    equal 1.0   # dt in fs
variable	DELAY       equal 100   # output data every # timesteps
variable        TOT_TIME    equal 100000 # run simulation for # timesteps in total

##################################################################################
#                                 Code                                           #
##################################################################################

units           real
atom_style      full
boundary        p p p

pair_style      lj/cut/tip4p/long 2 1 1 1 0.1250 8.5
bond_style      harmonic
angle_style     harmonic
kspace_style    pppm/tip4p       0.001

echo            screen

read_restart	restart.${LAST_TEMP}
reset_timestep	0

######################################################
# Coefficients for bonds, angles, diheadrals & pairs #
######################################################

pair_coeff      1       1	0.0	0.0
pair_coeff      1       2	0.0	0.0
pair_coeff      2       2	0.16275	3.188

bond_coeff      1       0.0     0.9572

angle_coeff     1       0.0     104.52

group water type 1 2
group oxy   type 2

#####################################################
# Neighbour list: distance = LJ cutoff + 2.0 = 10.5 #
#####################################################
neighbor        2.0 bin
###############################
# List renewed every timestep #
###############################
neigh_modify    delay 0 every 1 check no

#################
# SHAKE command #
#################
fix	2 water shake 1.0e-10 20 0 b 1 a 1
##############################
# Nose-Hoover thermobarostat #
##############################
fix     1 all npt temp ${TEMPERATURE} ${TEMPERATURE} 20.0 iso ${PRESSURE} ${PRESSURE} 30.0
#########################################
# Rebalance number of particles per CPU #
# Can speed up on muliple cores         #
#########################################
fix	bal all balance 10 1.05 shift xy 10 1.05

###################
# RDF computation #
###################
compute RDF oxy rdf 150
fix OUTRDF oxy ave/time 100 80 ${TOT_TIME} c_RDF[*] file rdf.${TEMPERATURE} mode vector

timestep ${TIMESTEP}

#########################################
# Commented out: creates trajectory     #
# Can visualize trajectory by using VMD #
#########################################
#dump            1 all custom 100 WATER.lammpstrj id type x y z vx vy vz
#dump_modify     1 sort id

variable	red_enth equal enthalpy/2500.0
variable	red_ke equal ke/2500.0

thermo_style    custom step temp etotal v_red_ke v_red_enth vol lx press
thermo ${DELAY}

run ${TOT_TIME}

write_restart	restart.${TEMPERATURE}
