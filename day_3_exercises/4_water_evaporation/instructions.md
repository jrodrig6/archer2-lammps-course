# Heating and cooling water

The aim of this exercise is to produce and analyse LAMMPS results to obtain interesting thermodynamic information. Users are asked to run a series of simulations of water at various temperatures, starting at 275 K and increasing in steps of 25 K to a temperature of 450 K. Users will then analyse the outputs from the LAMMPS thermo_style to learn properties of the system being studied.

## A bit about the simulation system

In this directory, you will find a LAMMPS input script `in.water` and a LAMMPS restart file `restart.275`. The restart file is the final configuration of a 1.0 nanosecond long simulation of 2500 [TIP3P water](https://lammps.sandia.gov/doc/Howto_tip3p.html) molecules at 275 K. This simulation was run for this long to ensure that the system has reached thermodynamic equilibrium. For those interested in looking at data files, I have included the original LAMMPS data file and associated input script in the `data_file_run` subdirectory.

At the top of `in.water`, there are a number of variables with which you can play. These are:

 * `TEMPERATURE` defines the temperature in Kelvin of this simulation run. It will be used for the thermostat and to name the LAMMPS restart file.
 * `LAST_TEMP` should be the temperature of the restart file you plan to use. If you've changed the way you name your restart files, you have no real need for this variable
 * `PRESSURE` defines the pressure for your system. The pressure is set at 1 atm.
 * `TIMESTEP` defines the length of your timestep in femtoseconds.
 * `DELAY` defines how many timesteps you want to wait between each 'thermo' output
 * `TOT_TIME` defines the total number of timesteps you want the simulation to run for. For this system, a run of 100,000 timesteps will take 2-8 minutes on a single ARCHER node (using all 24 processors).

The properties output to screen are:

 * `step` -- the current simulation timestep.
 * `temp` -- the current simulation temperature (calculated from the translational kinetic energy).
 * `etotal` -- the current total energy of the system.
 * `v_red_ke` -- the current average per-particle kinetic energy of the system. The `v_*` tells you that this is a variable defined in the input script.
 * `v_red_enth` -- the current average per-particle enthalpy of the system.
 * `vol` -- the volume of the box in Angstroms^3.
 * `lx` -- the length of one of the sides of the box in Angstroms. As the box is cubic, one side is enough to give us the length of all sides.
 * `press` -- the current system pressure in atmospheres.

## Exercise questions

 1. To begin, run the simulation "as is" in the exercise directory. This will run a simulation that uses the file `restart.275` as the starting input, and runs a 0.1 ns simulation at 275 K. This simulation will output a log file, a file giving the ozygen-oxygen radial distribution function (RDF) for the last fifth of the simulation, and a new restart file (note that this new restart file will overwrite the original `restart.275` for this case only).
 2. Make a copy of the LAMMPS log file and the RDF file.
 3. Change the input file to define the new simulation temperature as being 300 K (you can do this by changing the variable `TEMPERATURE`). Also, ensure that the variabel `OLD_TEMP` is equal to the temperature of the last simulation you ran (in this case 275 K). Run the the new simulation
 4. Make a copy of the LAMMPS log file and the RDF file
 5. Repeat steps 2 and 3 while increasing the simulation temperature by 25 K each time. Make sure to change both the `TEMPERATURE` and `LAST_TEMP` variable appropriately). Keep doing this up to a temperature of 450-500 K

We will now be looking at the thermodynamic values averaged over an entire simulation run. To begin, find the simulation average of the output thermodynamic properties for each temperature run and put them in a table. 

| Temp. / K | E<sub>tot</sub>  / kcal mol<sup>-1</sup> | K.E. / kcal mol<sup>-1</sup> | Enth. / kcal mol<sup>-1</sup>| Vol. / nm<sup>3</sup> | Press / atm|
| :------: | :------: | :------: | :------: | :------: | :------: |
| 275 | | | | | |

Once you have filled this table, plot the enthalpy and temperature as functions of temperature. What do the results show?

Plot the RDFs from the lowest and highest temperature runs. How do they compare?

If you are feeling adventurous, compare these results with a set of runs where you only use the `restart.275` file. How similar are the results?
 
**Note** This exercise is inspired by a previous LAMMPS exercise developed by Philip J. Camp. It is reproduced here with his permission.